import axios from "axios";

const COURSE_API_URL = 'http://localhost:8080'
const INSTRUCTOR_API_URL = `${COURSE_API_URL}/instructors`

class CourseDataService {
    retrieveAllCourses(name) {
        return axios.get(`${INSTRUCTOR_API_URL}/${name}/courses`);
    }

    retrieveCourse(name, id) {
        return axios.get(`${INSTRUCTOR_API_URL}/${name}/courses/${id}`);
    }

    createCourse(name, course) {
        return axios.post(`${INSTRUCTOR_API_URL}/${name}/courses/`, course);
    }

    updateCourse(name, id, course) {
        return axios.put(`${INSTRUCTOR_API_URL}/${name}/courses/${id}`, course);
    }

    deleteCourse(name, id) {
        return axios.delete(`${INSTRUCTOR_API_URL}/${name}/courses/${id}`);
    }
}

export default new CourseDataService();
