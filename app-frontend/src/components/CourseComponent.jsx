import {Component} from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import CourseDataService from "../services/CourseDataService";
import {INSTRUCTOR} from "./ListCoursesComponent";

class CourseComponent extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            id: this.props.match.params.id,
            description: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.validate = this.validate.bind(this);
    }

    componentDidMount() {
        if (this.state.id === 'new') {
            return;
        }

        CourseDataService.retrieveCourse(INSTRUCTOR, this.state.id)
            .then(response => this.setState({
                description: response.data.description
            }));
    }

    validate(values) {
        let errors = {};
        if (!values.description) {
            errors.description = 'Please, enter a description';
        } else if (values.description.length < 5) {
            errors.description = 'Description must be at least 5 characters long';
        }
        return errors;
    }

    onSubmit(values) {
        let username = INSTRUCTOR;

        let course = {
            id: this.state.id,
            username: username,
            description: values.description,
            targetDate: values.targetDate
        }

        if (this.state.id === 'new') {
            CourseDataService.createCourse(username, {...course, id: -1})
                .then(() => this.props.history.push('/courses'));
        } else {
            CourseDataService.updateCourse(username, this.state.id, course)
                .then(() => this.props.history.push('/courses'));
        }
    }

    render() {
        let { description, id } = this.state;

        return (
            <div>
                <h3>Course</h3>
                <div className="container">
                    <Formik
                        initialValues={{ id, description }}
                        onSubmit={this.onSubmit}
                        validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}
                    >
                        {
                            () => (
                                <Form>
                                    <ErrorMessage name="description" component="div" className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>Id</label>
                                        <Field className="form-control" type="text" name="id" disabled />
                                    </fieldset>
                                    <fieldset>
                                        <label>Description</label>
                                        <Field className="form-control" type="text" name="description" />
                                    </fieldset>
                                    <button className="btn btn-success" type="submit">Save</button>
                                </Form>
                            )
                        }
                    </Formik>
                </div>
            </div>
        )
    }
}

export default CourseComponent;
