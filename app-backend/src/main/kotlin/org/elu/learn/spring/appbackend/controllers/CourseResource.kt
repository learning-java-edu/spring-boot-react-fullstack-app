package org.elu.learn.spring.appbackend.controllers

import org.elu.learn.spring.appbackend.models.Course
import org.elu.learn.spring.appbackend.services.CoursesHardcodedService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.servlet.support.ServletUriComponentsBuilder

@CrossOrigin(origins = ["http://localhost:3000", "http://localhost:4200"])
@RestController
class CourseResource(val courseManagementService: CoursesHardcodedService) {
    @GetMapping("/instructors/{username}/courses")
    fun getAllCourses(@PathVariable username: String) = courseManagementService.findAll()

    @GetMapping("/instructors/{username}/courses/{id}")
    fun getCourse(@PathVariable username: String, @PathVariable id: Long) =
        courseManagementService.findById(id)

    @PostMapping("/instructors/{username}/courses")
    fun createCourse(@PathVariable username: String, @RequestBody course: Course) =
        courseManagementService.save(course).let {
            val uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(it.id)
                .toUri()
            ResponseEntity.created(uri).build<Void>()
        }

    @PutMapping("/instructors/{username}/courses/{id}")
    fun updateCourse(@PathVariable username: String, @PathVariable id: Long, @RequestBody course: Course) =
        ResponseEntity<Course>(courseManagementService.save(course), HttpStatus.OK)

    @DeleteMapping("/instructors/{username}/courses/{id}")
    fun deleteCourse(
        @PathVariable username: String,
        @PathVariable id: Long
    ): ResponseEntity<Void> = try {
        courseManagementService.deleteById(id).let {
            ResponseEntity.noContent().build()
        }
    } catch (e: NoSuchElementException) {
        ResponseEntity.notFound().build()
    }
}
