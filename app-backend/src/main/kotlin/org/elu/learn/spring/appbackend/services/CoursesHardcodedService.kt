package org.elu.learn.spring.appbackend.services

import org.elu.learn.spring.appbackend.models.Course
import org.springframework.stereotype.Service

@Service
class CoursesHardcodedService {
    private final val courses = mutableListOf<Course>()
    private final var idCounter = 0L

    init {
        courses.add(Course(++idCounter, "in28minutes", "Learn Full stack with Spring Boot and Angular"))
        courses.add(Course(++idCounter, "in28minutes", "Learn Full stack with Spring Boot and React"))
        courses.add(Course(++idCounter, "in28minutes", "Master Microservices with Spring Boot and Spring Cloud"))
        courses.add(
            Course(
                ++idCounter, "in28minutes",
                "Deploy Spring Boot Microservices to Cloud with Docker and Kubernetes"
            )
        )
    }

    fun findAll(): List<Course> = courses

    fun save(course: Course) = if (course.id < 1) {
        val newCourse = Course(++idCounter, "in28minutes", course.description)
        courses.add(newCourse)
        newCourse
    } else {
        courses.find { it.id == course.id }?.description = course.description
        course
    }

    fun deleteById(id: Long): Course = findById(id).let {
        return if (courses.remove(it)) {
            it
        } else {
            throw NoSuchElementException("Element not found by id $id")
        }
    }

    fun findById(id: Long): Course {
        return courses.find { it.id == id } ?: throw NoSuchElementException("Element not found by id $id")
    }
}
