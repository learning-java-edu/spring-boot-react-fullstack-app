package org.elu.learn.spring.appbackend.models

data class Course(val id: Long, var username: String, var description: String)
